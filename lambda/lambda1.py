
# lambda
x = 5

code_execute_print_with_hello = lambda: print("hello world!")

code_execute_print_with_hello()

def above_8(x):
    return x > 8

l1 = [1,2,3,4,5,6,7,8,9]
print(list(filter( above_8, l1 )))

def even(x):
    return x % 2 == 0
l1 = range(100)
print(list(filter( even, l1 )))
print(list(filter( lambda x: x % 2 == 0, l1)))
print(list(filter( lambda x: x % 2 != 0, l1)))
print(list(filter( lambda x: x % 7 == 0, l1)))
print(list(filter( lambda x: x > 50, l1)))

l1 = range(9)
def power2(x):
    return x * x
# 1*1=1 2*2=4 3*3=9 4*4=16 ...
print(list(map(power2, l1)))

# 1-100 and return each number divided by 2
#    [2,4,10,20] ==> [1,2,5,10]
# [1,2,4,7,9] == [false,true,true,false]
#   1) map (no lambda) : x return True if even otherwise False
#   2) map with lambda
# p = [ ("itay", 1) , ("Danny", 2) , ("Maya", 5") ]
#   map lambad == > ["ITAY", "DANNY", "MAYA" ]
# [-5,9,100,-4,201]
#   return the list only with positive numbers and divided by 3
#       [3,33.3,66.6]
# [ [1], [1,2,3], [-5,7,12,50,100] ]
#   return [1, 3, 5]  (the len of each list)
# p = [ "Clark Keny", "Danny Din", "Bruce Wane" ]
#   result = ["Clark", "Danny", "Bruce"]

# 1 ,  5 ,  8 ,100
# 2 , 10 , 16, 200






